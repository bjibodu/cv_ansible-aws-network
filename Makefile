MAKEFILE_PATH:= $(abspath $(lastword $(MAKEFILE_LIST)))
MAKEFILE_ROOT:= $(dir $(MAKEFILE_PATH))

MANDATORY_VARS:=\
	TAG_ENV

REQUIRED_TOOLS:=\
	PRE_COMMIT \
	TERRAFORM \
	ANSIBLE \
	ANSIBLE_GALAXY \
	ANSIBLE_PLAYBOOK \
	GIT

export AWS_DEFAULT_REGION:=eu-west-2


ANSIBLE_VERBOSE?=-vv
ANSIBLE_TAGS?=all
TERRAFORM_VERSION?=0.11.5
TERRAFORM_BIN_PATH?=/usr/local/terraform/$(TERRAFORM_VERSION)/terraform
TERRAFORM:=$(shell command -v $(TERRAFORM_BIN_PATH) 2>/dev/null)
TERRAFORM_ARGS?=

TERRAFORM_VAR_ENV_FILE:=$(TAG_ENV)


PRE_COMMIT:=$(shell command -v pre-commit 2>/dev/null)
ANSIBLE:=$(shell command -v ansible 2>/dev/null)
ANSIBLE_GALAXY:=$(shell command -v ansible-galaxy 2>/dev/null)
ANSIBLE_PLAYBOOK:=$(shell command -v ansible-playbook 2>/dev/null)
GIT:=$(shell command -v git 2>/dev/null)

$(foreach A, $(REQUIRED_TOOLS), \
	$(if $(value $A),, $(error $A could not be found in $(PATH))) \
)

ifdef MAKE_DEBUG
$(info $(shell /bin/env))
endif

.DEFAULT_GOAL := help
.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.PHONY: pre
pre: clean_up ansible_galaxy ## 010 - Prerequisites


.PHONY: clean_up  ## 020 - Cleanup the local env
clean_up:
	$(info ... clean up)
	rm -rf .ansible_generated_terraform

.PHONY: ansible-galaxy
ansible_galaxy:
	$(info ... installing ansible requirements)
	$(ANSIBLE_GALAXY) install -r requirements.yml -f

.PHONY: terraform_gen
terraform_gen:
	$(info ... generating Terraform code)
	$(ANSIBLE_PLAYBOOK) $(ANSIBLE_VERBOSE) \
	-i "127.0.0.1," -c local playbook.yml --tags all

# TBD we need to fix the other projects. Currently this Makefile checks only the VPCs

.PHONY: terraform_lint_vpc
.ONESHELL:
terraform_lint_vpc:
	for project in $(MAKEFILE_ROOT)/.ansible_generated_terraform/vpc/* ; do \
		cd $$project ; \
		echo "Working inside: " $$project ; \
		$(TERRAFORM) init -input=false || (echo "Terraform init failed $$?"; exit 1); \
		$(TERRAFORM) validate || (echo "Terraform validate failed $$?"; exit 1); \
		cd ..; \
  done || exit 1

.PHONY: ci
ci: clean_up ansible_galaxy terraform_gen terraform_lint_vpc

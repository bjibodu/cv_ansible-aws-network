provider "aws" {
  region  = "eu-west-2"
  version = "1.11.0"
}

# SES is only available in us-east-1, us-west-2, and eu-west-1
# so that, I've created a hardcoded provider to verify domains
# to be able to use SES.
# Apart from this, all the resources will be created either
# global (such as Route53, IAM) or eu-west-2
provider "aws" {
  alias   = "eu-west-2"
  region  = "eu-west-2"
  version = "1.11.0"
}

provider "template" {
  version = "~> 0.1"
}

provider "archive" {
  version = "~> 0.1"
}

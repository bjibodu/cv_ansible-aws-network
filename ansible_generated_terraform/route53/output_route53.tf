// network details
output "management_ns_servers" {
  value = "${module.route53_setup.management_dns_ns}"
}

output "private_apex_dns_zone_id" {
  value = "${module.route53_setup.private_apex_dns_zone_id}"
}

output "management_dns_zone_id" {
  value = "${module.route53_setup.management_dns_zone_id}"
}

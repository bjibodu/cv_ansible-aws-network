terraform {
  required_version = "0.11.5"

  backend "s3" {
    bucket         = "cv-aws-network"
    key            = "cv-network.tfstate"
    region         = "eu-west-2"
    dynamodb_table = "cv-aws-network-lock"
    encrypt        = true
  }
}
